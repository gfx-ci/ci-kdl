# Continuous Integration - Kernel Data Logger

This project aims to provide a simple daemon tool that can log at 10Hz 
variables from the kernel, like the thermal sensors, the GPU frequencies, or 
the recently proposed memory usage evolution.

The original scope is to accommodate one more element in  
[mesa's init-stage2](https://gitlab.freedesktop.org/mesa/mesa/-/blob/main/.gitlab-ci/common/init-stage2.sh).
Once this element is launched, this tool collects data, smartly stores it, 
and saves it in the artifacts when each job is completed.

[[_TOC_]]

## Environment setup

A little __howto__ to set up the virtual environment in [python.md](./python.md). 
Once the environment is set up, one can launch the command:

```bash
ci-kdl
```

## Usage

- [ ] Check what's available to be logged
- [ ] Daemon mode:
  - [ ] Configure what the daemon will collect
  - [ ] Configure how the daemon will store the collected data
  - [ ] Configure the frequency to collect the requested data

## JSON output

Let's explain a sample taken from an acquisition with `ci-kdl`:

```json
{
  "version": 0.1, 
  "start_timestamp": "2023-06-20 12:45:12.486649", 
  "samples_number": 20, 
  "wait_time_in_milliseconds": 100, 
  "sampling_intervals": [[0, 0], [100, 100]], 
  "data": 
  {
    "TSKN": [[470, 0]], 
    "INT3400 Thermal": [[200, 0]], 
    "iwlwifi_1": [[327680, 0]], 
    "TAMB": [[450, 0]], 
    "TVR": [[470, 0]], 
    "TMEM": [[430, 0]], 
    "x86_pkg_temp": [[800, 0], [490, 100], [480, 200], [470, 400], [480, 100], [470, 500], [480, 601]], 
    "THP": [[480, 0], [470, 300]], 
    "TCPU": [[540, 0], [520, 300], [470, 1001]]
  }
}
```

All the data pairs are `(value, relative_ts)` where relative timestamp points 
to the previous record. And the first record is referenced to the time when the 
acquisition started.

Then for the Thermal Sensor with `type` "TCPU", it first recorded 54.0ºC when 
the acquisition started. The change to 52.0ºC happened 300ms later. The third 
record of 47ºC happened 1001ms after the second record (1001+300 after the 
`start_timestamp`).

Thermal reading exceptions from the corresponding file in sysfs stores a "out 
of range" value. This is a PLC-like way to report a reading error by reporting 
and impossible temperature. In this sample it is the case of "iwlwifi_1" that 
rejects all the readings, so it stored an impossible temperature of 32768.0ºC.
