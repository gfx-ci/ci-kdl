#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023-2024 Collabora Ltd"


from .constants import (
    DEFAULT_WAIT_INTERVAL_IN_MILLISECONDS,
    MINIMUM_WAITTIME_IN_MILLISECONDS,
)
from datetime import datetime
from .interval import SamplingInterval
import logging
import numpy as np
from .sampler import Sampler
from signal import signal, SIGINT, SIGTERM
import time
from .timestamp import TimeStamp
from threading import Event
from typing import List

logger = logging.getLogger(__name__)


class Acquirer:
    __sampling_interval: SamplingInterval = None
    __samplers: List[Sampler] = None
    __n_samples: int = None
    __current: int = None
    __wait_time_in_microseconds: int = None
    __acq_time_start: datetime = None
    __abort: Event = None

    def __init__(self, samplers: List[Sampler]):
        self.__sampling_interval = SamplingInterval()
        self.__samplers = samplers
        self.__abort = Event()
        signal(SIGINT, self.abort)
        signal(SIGTERM, self.abort)
        self.__abort.clear()

    @property
    def samples(self) -> int:
        return self.__current

    @property
    def wait_time_in_milliseconds(self) -> int:
        return self.__wait_time_in_microseconds // 1000

    @property
    def acquisition_started_at(self) -> datetime:
        return self.__acq_time_start

    @property
    def sampling_intervals(self) -> np.array:
        return self.__sampling_interval.acquired_values

    @property
    def acquired_values(self) -> dict:
        return {sampler.name: sampler.acquired_values for sampler in self.__samplers}

    def prepare(
        self,
        n_samples: int,
        wait_time_in_milliseconds: int = DEFAULT_WAIT_INTERVAL_IN_MILLISECONDS,
    ) -> None:
        assert wait_time_in_milliseconds >= MINIMUM_WAITTIME_IN_MILLISECONDS
        self.__n_samples = n_samples
        self.__wait_time_in_microseconds = wait_time_in_milliseconds * 1000
        for sampler in self.__samplers:
            sampler.prepare(self.__n_samples)
        self.__sampling_interval.prepare(n_samples)

    def keep_sampling(self) -> bool:
        if self.__abort.is_set():
            return False

        if self.__n_samples > 0 and self.__current >= self.__n_samples:
            return False

        return True

    def acquire(self) -> None:
        self.__acq_time_start = datetime.now()
        self.__current = 0
        self.__abort.clear()
        logger.debug(f"Acquisition start at {self.__acq_time_start}")
        while self.keep_sampling():
            # the acquisition:
            t_0 = TimeStamp(datetime.now())
            for sampler in self.__samplers:
                sampler.acquire_one(t_0)
            self.__sampling_interval.acquire_one(t_0)
            self.__current += 1
            t_diff = (TimeStamp.now() - t_0).microseconds
            next_acq_in = self.__wait_time_in_microseconds - t_diff
            if next_acq_in > 0:
                time.sleep(next_acq_in / 1000000)
        logger.debug(
            f"Acquisition completed at {datetime.now()} with {self.__current} samples."
        )

    def abort(self, *args):
        logger.debug(f"Signal {args[0]} received: abort")
        self.__abort.set()
