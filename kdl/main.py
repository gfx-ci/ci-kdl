#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .acquisition import Acquirer
from .constants import DEFAULT_WAIT_INTERVAL_IN_MILLISECONDS
from argparse import ArgumentParser, Namespace
from datetime import datetime
import logging
from pathlib import Path
from .recorder import Recorder
from .thermal_zone import ThermalZones
import time

logger = logging.getLogger("kdl")

def main():
    args = __cli_arguments()
    logging.basicConfig(level=args.log_level)
    __cmd_kdl(args)


def __cli_arguments() -> Namespace:
    """
    Define the command line behavior for the arguments and subcommands.
    :return: argument parser
    """
    parser = ArgumentParser(
        description="CLI tool to provide a tool for collecting several "
        "variables from the kernel during a period under a given "
        "frequency."
    )
    # TODO: how often it has to pull data to output file
    #  When a interruption signal is received last chunk must be written also.
    parser.add_argument(
        "--output-file",
        default=f"./kdl_{time.strftime('%Y%m%d_%H%M%S')}.json",
        type=Path,
        help="Filename for JSON content output "
        "(defaults to timestamped file in local directory)"
    )
    parser.add_argument(
        "--log-level",
        choices=[logging.getLevelName(x) \
        for x in [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL]],
        default=logging.getLevelName(logging.INFO))
    parser.add_argument(
        "--sampling-interval",
        type=int,
        default=DEFAULT_WAIT_INTERVAL_IN_MILLISECONDS,
        help="Number of milliseconds in between sampling"
    )
    parser.add_argument(
        "--num-samples",
        type=int,
        default=-1,
        help="Number of samples before exiting, or -1 to sample until interrupted"
    )
    return parser.parse_args()


def acquisition(
    n_samples: int,
    output_file: Path,
    wait_time_in_msec: int = None,
    factor: float = None,
) -> None:
    thermal_zones = ThermalZones(factor)
    acquirer = Acquirer(list(thermal_zones))
    acquirer.prepare(n_samples, wait_time_in_msec)
    acquirer.acquire()
    t_f = datetime.now()
    t_0 = acquirer.acquisition_started_at
    logger.info(f"Acquisition started at {t_0} ended at {t_f} ({t_f - t_0})")
    recorder = Recorder(acquirer, output_file=output_file)
    recorder.generate_json()
    logger.info(f"Results at {recorder.file_name}")


def __cmd_kdl(args: Namespace) -> None:
    # TODO: those checks must be transformed to unit tests
    # Just a read example:
    thermal_zones = ThermalZones()
    logger.debug(f"Thermal zones names: {thermal_zones.type}")
    logger.debug(f"* Single reading: {thermal_zones.temperatures}")

    # Next, some acquisition sets:
    # print("* 20 samples at default wait_time is 100ms, 10Hz: 2 seconds")
    # acquisition(20)
    # print("* Convert milli-degrees to deci-degrees")
    # acquisition(20, factor=1e-2)
    # print("* wait_time of 10ms, the minimum: 0.2 seconds")
    # acquisition(20, 10, factor=1e-2)
    # print("* wait_time of 20ms, 50Hz: 0.4 seconds")
    # acquisition(20, 20, factor=1e-2)
    # print("* Maximum frequency (100Hz) during 2 seconds")
    # acquisition(100, 10, factor=1e-2)
    # print("* Maximum frequency (100Hz) during 10 seconds")
    # acquisition(500, 10, factor=1e-2)
    # print("* 60 seconds at 10Hz readings but only archive when value changes")
    # acquisition(600)
    # print("* The same but reduce sensibility from milli-Celcius to deca-Celcius")
    # acquisition(600, factor=1e-2)
    # print(
    #     "* 5 minutes acquisition at 10Hz recording only relative changes in the deca-Celcius degree"
    # )
    # acquisition(3000, factor=1e-2)
    logger.debug(
        f"* Acquiring {args.num_samples} samples at {1000 / args.sampling_interval}Hz recording only relative changes in the deca-Celcius degree"
    )
    acquisition(args.num_samples, output_file=args.output_file,
                wait_time_in_msec=args.sampling_interval, factor=1e-2)


if __name__ == "__main__":
    main()
