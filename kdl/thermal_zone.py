#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .constants import UNREADABLE_MILLICELCIUS
import errno
from os import walk
from .sampler import Sampler
from typing import TextIO


_DEFAULT_THERMAL_ZOME_PATH = "/sys/class/thermal/thermal_zone{idx}"


class ThermalZone(Sampler):
    __idx: int = None
    __factor: float = None
    __file_descriptor: TextIO = None

    def __init__(self, idx: int, factor: float = None, *args, **kwargs):
        super(ThermalZone, self).__init__(*args, **kwargs)
        self.__path = _DEFAULT_THERMAL_ZOME_PATH.replace("{idx}", f"{idx}")
        self.__idx = idx
        self.__factor = factor
        self.__file_descriptor = open(f"{self.__path}/temp")

    def __del__(self):
        self.__file_descriptor.close()

    def __str__(self) -> str:
        return f"{self.__path}"

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.__idx})"

    @property
    def name(self) -> str:
        return self.type

    @property
    def value(self) -> int:
        return self.temperature

    @property
    def type(self) -> str:
        with open(f"{self.__path}/type") as f_descr:
            return f_descr.read().strip()

    @property
    def temperature(self) -> int:
        if self.__factor:
            return int(self.__read_value() * self.__factor)
        return self.__read_value()

    @property
    def factor(self) -> float:
        return self.__factor

    def __read_value(self):
        try:
            self.__file_descriptor.seek(0)
            return int(self.__file_descriptor.read())
        except OSError as exception:
            if exception.errno == errno.ENODATA:  # 'No data available'
                return UNREADABLE_MILLICELCIUS
            raise exception


class ThermalZones:
    __thermal_zones: dict = None

    def __init__(self, factor: float = None):
        self.__thermal_zones = {}
        basedir, subdir = _DEFAULT_THERMAL_ZOME_PATH.rsplit("/", 1)
        subdir_pattern = subdir.split("{idx}")[0]
        for _, names, _ in walk(basedir):
            for name in names:
                if name.startswith(subdir_pattern):
                    idx = int(name.split(subdir_pattern)[1])
                    self.__thermal_zones[idx] = ThermalZone(idx, factor)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}()"

    @property
    def temperatures(self) -> list:
        return [
            thermal_zone.temperature for thermal_zone in self.__thermal_zones.values()
        ]
        # TODO: is this memory efficient to be requested at 10Hz?

    @property
    def type(self) -> list:
        return [thermal_zone.type for thermal_zone in self.__thermal_zones.values()]

    def __len__(self) -> int:
        return len(self.__thermal_zones)

    def __getitem__(self, item) -> Sampler:
        return list(self.__thermal_zones.values())[item]
