#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


import numpy as np
from .timestamp import TimeStamp
from typing import Union


class Sampler:
    """
    Abstract class for objects that can be included into an Acquirer.
    """

    __data: np.ndarray = None
    __current: int = None
    __factor: float = None
    __reference_value: Union[int, float] = None  # used in relative archiving
    __reference_time: TimeStamp = None

    @property
    def name(self) -> str:
        """
        Abstract method to identity a sampler object
        :return:
        """
        raise NotImplementedError

    @property
    def value(self) -> Union[int, float]:
        """
        Abstract method to record a value from a sampler object
        :return:
        """
        raise NotImplementedError

    @property
    def acquired_values(self) -> np.array:
        # TODO: raise an exception if no acquisition completed
        return np.asarray(self.__data)

    def prepare(self, n_samples: int):
        self.__data = []
        self.__current = 0

    def acquire_one(self, ts: TimeStamp) -> None:
        new_value = self.value
        if self.__reference_value != new_value:
            if not self.__reference_time:
                t_step = 0
            else:
                t_step = (ts - self.__reference_time).milliseconds
            self.__data.append((new_value, t_step))
            self.__current += 1
            self.__reference_time = ts
            self.__reference_value = new_value
