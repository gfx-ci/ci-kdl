#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .acquisition import Acquirer
import json
from json import JSONEncoder
from pathlib import Path
import numpy as np
from typing import List


class _NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


class Recorder:
    __acquirer: Acquirer = None
    __output_file: Path = None

    def __init__(self, acquirer: Acquirer, output_file: Path):
        self.__acquirer = acquirer
        self.__output_file = output_file

    @property
    def file_name(self) -> str:
        return f"{self.__output_file.absolute()}"

    def generate_json(self):
        output = json.dumps(
            {
                "version": 0.1,
                "start_timestamp": f"{self.__acquirer.acquisition_started_at}",
                "samples_number": self.__acquirer.samples,
                "wait_time_in_milliseconds": self.__acquirer.wait_time_in_milliseconds,
                "sampling_intervals": self.__acquirer.sampling_intervals,
                "data": self.__acquirer.acquired_values,
            },
            cls=_NumpyArrayEncoder,
        )
        with open(self.__output_file, "w") as f:
            f.write(output)
