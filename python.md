# Python environment

## Python using virtualenv

Based on python 3.9, one can setup a virtual environments. 

```bash
$ pip3 -V
pip 22.3.1 from /usr/lib/python3/dist-packages/pip (python 3.9)
$ python3 -m pip install virtualenv
```

A virtual environment can be created in order to use this module: 

```bash
$ python3 -m virtualenv ci-kdl.venv
$ source ci-kdl.venv/bin/activate
(...)
$ python -m pip install numpy ruamel.yaml tenacity
$ pip list
Package          Version
---------------- -------
numpy            1.24.3
pip              23.1.2
ruamel.yaml      0.17.26
ruamel.yaml.clib 0.2.7
setuptools       67.7.2
tenacity         8.2.2
wheel            0.40.0
```

To reproduce the same virtual environment that this tools is being tested:

```bash
$ pip install -r requirements.txt
```

### Install module

To install this module in the virtual environment in a way that changes do not 
require to reinstall it:

```bash
pip install -e .
```

Then one will have available the command:

```bash
ci-kdl
```

### Export changes on the environment

In case some newer packages are required, the easiest way to mark for others 
to use is:

```bash
pip freeze --all > requirements.txt
```

